const fs = require('fs');
var rp = require('request-promise');
const crypto = require('crypto');
const { baseUrl, urls } = require('./config');

const md5 = data => crypto.createHash('md5').update(data).digest("hex");

const pathFromUrl = url => `${__dirname}/cache/${md5(url)}.json`;

const reqUrl = (url) => {
  console.log(`Fetching: ${baseUrl + url}`);
  rp(baseUrl + url).then(data => {
    fs.writeFileSync(pathFromUrl(url), data);
    return data;
  }).catch((err) => console.error(err));
}

module.exports = {
  getCached: async (url) => {
    fs.mkdir(__dirname + '/cache', { recursive: true }, (err) => {
      if (err) throw err;
    });
    const filepath = pathFromUrl(url);
    if (!fs.existsSync(filepath)) {
      try {
        const data = await reqUrl(url);
        return JSON.parse(data);
      } catch (error) {
        return {};
      }
    }
    const data = fs.readFileSync(filepath);
    return JSON.parse(data);
  },
}