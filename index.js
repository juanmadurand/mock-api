const express = require('express');

const { port } = require('./config');
const { getCached } = require('./lib');

const app = express();

app.get('/api/*', (req, res) => {
  const url = req.url.replace(/(\&filter\[1\]\[condition\]\[value\]\=)([0-9]{10})/ig, '$1=1576192547');
  getCached(url).then(data => res.json(data));
});

app.listen(port, () => console.log(`Mock listening on port ${port}!`));